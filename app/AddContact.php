<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddContact extends Model
{
    protected $fillable = [
        'Name', 'Email', 'ContactNo', 'City', 'Address',
    ];
}
