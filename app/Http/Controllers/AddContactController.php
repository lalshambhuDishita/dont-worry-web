<?php

namespace App\Http\Controllers;

use Mail;
use App\AddContact;
use Illuminate\Http\Request;

class AddContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();
        return $items;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //echo '<pre>';print_r($request->Name);die;
        $to_name = "Techzapp Solutions";
        $to_email = "techzapp.shambhu@gmail.com";

        // $data = array(
        //     'name'=> $request->Name,
        //     'email'=> $request->Email,
        //     'subject'=> "test",
        //     'body' => $request->ContactNo." ".$request->City." <br/>".$request->Address
        // );
        // try{
        //     //return AddContact::create($request->all());
        //     Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
        //         $message->to($to_email, $to_name)->subject('Laravel Test Mail updated');
        //         $message->from('no-reply@techzapp.com','Techzapp Solutions');
        //     });
            
        // }catch(Exception $error){
        //     dd($error);
        // } 
           return AddContact::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AddContact  $addContact
     * @return \Illuminate\Http\Response
     */
    public function show(AddContact $addContact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AddContact  $addContact
     * @return \Illuminate\Http\Response
     */
    public function edit(AddContact $addContact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AddContact  $addContact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AddContact $addContact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AddContact  $addContact
     * @return \Illuminate\Http\Response
     */
    public function destroy(AddContact $addContact)
    {
        //
    }
}
