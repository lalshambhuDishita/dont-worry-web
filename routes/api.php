<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/api/storeItem', 'AddContactController@store');
Route::put('/api/updateItem', 'AddContactController@update');
Route::delete('/api/deleteItem/{id}', 'AddContactController@destroy');
Route::get('/api/itemsList', 'AddContactController@index');

Route::post('/api/storeToken', 'TokenController@store');
