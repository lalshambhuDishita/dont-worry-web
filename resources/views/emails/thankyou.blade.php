@extends('layouts.emaillayout')



<table cellpadding="0" cellspacing="0" width="50%" style="border:2px solid #000000;">
<tr style="border:1px solid #000000;background-color:#00c7e6;width:100%;text-align:center;">
    <th><img style="width:200px;height:50px;" src="{{ $message->embed('http://www.techzapp.com/img/logo.png') }}"></th>
</tr>
<tr style="width:100%;text-align:center;">
    <th><p><strong>Thank you for getting in touch!</strong></p></th>
</tr>
 <tr>
  <td style="padding:20px;">
  <p>Dear {{$name}}</p>
   <p>Thanks for the feedback on your experience with our customer support team. We sincerely appreciate your insight because it helps us build a better customer experience.</p>
   <p>If you have any more questions, comments, or concerns or compliments, please feel welcome to reach back out as we would be more than happy to assist.</p>
   <p>Best,</p>
   <p>Techzapp Solutions</p>
  </td>
 </tr>
 <tr style="border:2px solid #000000;background-color:#dddddd;width:100%;text-align:center;">
    <th><p>Techzapp Solutions All rights reserved.</p></th>
</tr>
</table>