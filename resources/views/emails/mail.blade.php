@extends('layouts.emaillayout')



<table cellpadding="0" cellspacing="0" width="50%" style="border:2px solid #000000;">
<tr style="border:1px solid #000000;background-color:#00c7e6;width:100%;text-align:center;">
    <th><img style="width:200px;height:50px;" src="{{ $message->embed('http://www.techzapp.com/img/logo.png') }}"></th>
</tr>
<tr style="width:100%;text-align:center;">
    <th><p>Hello Support Team,

Below person is trying to reach you.</p></th>
</tr>
 <tr>
  <td width="260" valign="top">
   <table  cellpadding="0" cellspacing="0" width="100%" style="margin-left:20px;padding:15px;">
   <tr>
        <td style="min-width:200px"><b>Name : </b></td>
        <td style="padding:10px">{{$name}}</td>
    </tr>
    <tr>
        <td><b>Email Contact : </b></td>
        <td style="padding:10px">{{$email}}</td>
    </tr>
    <tr>
        <td><b>Message : </b></td>
        <td style="padding:10px;">{{$body}}</td>
    </tr>
   </table>
  </td>
 
  
 </tr>
 <tr style="border:2px solid #000000;background-color:#dddddd;width:100%;text-align:center;">
    <th><p>Techzapp Solutions All rights reserved.</p></th>
</tr>
</table>